package com.example.github

import java.io.Serializable

data class Repository(
    val id: Long,
    val name: String,
    val description: String?,
    val html_url: String,
    val forks: Int,
    val watchers: Int,
    val issues: Int,
    val parent: RepositoryParent?
) : Serializable

data class RepositoryParent(
    val full_name: String
) : Serializable
