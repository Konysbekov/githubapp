package com.example.github

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val repository: Repository? = intent.getParcelableExtra("repository") ?: null


        findViewById<TextView>(R.id.repoDetailName).text = repository?.name
        findViewById<TextView>(R.id.repoDetailParent).text = "Parent: ${repository?.parent}"
        findViewById<TextView>(R.id.repoDetailForks).text = "Forks: ${repository?.forks}"
        findViewById<TextView>(R.id.repoDetailWatchers).text = "Watchers: ${repository?.watchers}"
        findViewById<TextView>(R.id.repoDetailIssues).text = "Issues: ${repository?.issues}"
        findViewById<TextView>(R.id.repoDetailDescription).text =
            "Description: ${repository?.description}"
    }
}
