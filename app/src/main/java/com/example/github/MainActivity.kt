package com.example.github

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RepositoryAdapter
    private lateinit var editTextOrganization: EditText
    private lateinit var buttonSearch: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.recyclerView)
        editTextOrganization = findViewById(R.id.editTextOrganization)
        buttonSearch = findViewById(R.id.buttonSearch)
        recyclerView.layoutManager = LinearLayoutManager(this)

        buttonSearch.setOnClickListener {
            val organizationName = editTextOrganization.text.toString()
            if (organizationName.isNotEmpty()) {
                fetchRepos(organizationName)
            } else {
                Toast.makeText(this, "Please enter an organization name", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun fetchRepos(organizationName: String) {
        RetrofitClient.instance.listRepos(organizationName)
            .enqueue(object : Callback<List<Repository>> {
                override fun onResponse(call: Call<List<Repository>>, response: Response<List<Repository>>) {
                    if (response.isSuccessful) {
                        Log.i("response", "onResponse:$response ")
                        val repos = response.body()
                        if (repos != null)
                        adapter = RepositoryAdapter(repos) { repository ->
                            val intent = Intent(this@MainActivity, DetailActivity::class.java)
                            intent.putExtra("repository", repository)
                            startActivity(intent)
                        }
                        recyclerView.adapter = adapter
                    } else {
                        Toast.makeText(this@MainActivity, "Failed to fetch repositories", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onFailure(call: Call<List<Repository>>, t: Throwable) {
                    Toast.makeText(this@MainActivity, "Failed to fetch repositories", Toast.LENGTH_SHORT).show()
                }
            })
    }
}
